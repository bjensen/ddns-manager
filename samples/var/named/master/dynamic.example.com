$ORIGIN .
$TTL 86400      ; 1 day
dynamic.example.com  IN SOA  ns.example.com. dnsadmin.example.com. (
                                201604087  ; serial
                                10800      ; refresh (3 hours)
                                3600       ; retry (1 hour)
                                1814400    ; expire (3 weeks)
                                86400      ; minimum (1 day)
                                )
                        NS      ns.example.com.
$ORIGIN dynamic.example.com
$TTL 60 ; 1 minute
dynhost1                A       192.168.54.86
