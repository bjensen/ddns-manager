<?php
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;

    require '../../vendor/autoload.php';

    spl_autoload_register(function ($classname) {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $classname);
        require ('../lib/' . $path . '.php');
    });

    $config = array(
        'displayErrorDetails' => \Ddns\Config::getGlobal('debug'),
    );

    $app = new \Slim\App($config);

    $app->get('/auto', function(Request $request, Response $response){
        $requiredParams = array('domain', 'user', 'key');
        $requestParams = $request->getQueryParams();

        // Verifying that all require parameters are present
        foreach ($requiredParams as $param) {
            if (!array_key_exists($param, $requestParams)) {
                // Missing a required parameter, respond with status code 400 (Bad Request)
                $response->getBody()->write('Bad Request: Invalid parameters.');
                return $response->withStatus(400);
            }
        }

        $domainBreakdown = explode(".", $requestParams['domain']);
        $host = $domainBreakdown[0];
        array_shift($domainBreakdown);
        $zone = implode(".", $domainBreakdown);
        $ip = $_SERVER['REMOTE_ADDR'];

        if (\Ddns\Config::hasPermission($requestParams['user'], $requestParams['key'], $zone, $host)) {
            // Update DNS Record
            $update = new \Ddns\NsUpdate();
            $update->setServer(\Ddns\Config::getGlobal('server_address'));
            $update->setZone($zone);
            if (\Ddns\Config::zoneNeedsKey($zone)) {
                $update->setKey(\Ddns\Config::getZoneKeyName($zone), \Ddns\Config::getZoneKeySecret($zone));
            }
            $update->deleteRecord($requestParams['domain'], null, null, 'A');
            $update->addRecord($requestParams['domain'], 60, 'A', $ip);
            $update->send();

            return $response->getBody()->write('Updated!');
        }
        else {
            return $response->withStatus(403);
        }
    });

    $app->get('/update', function (Request $request, Response $response) {
        // The parameters required to update the dns record
        $requiredParams = array('zone', 'host', 'ip', 'user', 'key');
        $requestParams = $request->getQueryParams();

        // Verifying that all require parameters are present
        foreach ($requiredParams as $param) {
            if (!array_key_exists($param, $requestParams)) {
                // Missing a required parameter, respond with status code 400 (Bad Request)
                $response->getBody()->write('Bad Request: Invalid parameters.');
                return $response->withStatus(400);
            }
        }

        if (\Ddns\Config::hasPermission($requestParams['user'], $requestParams['key'], $requestParams['zone'], $requestParams['host'])) {
            // Update DNS Record
            $update = new \Ddns\NsUpdate();
            $update->setServer(\Ddns\Config::getGlobal('server_address'));
            $zone = $requestParams['zone'];
            $hostForRecord = $requestParams['host'] . '.' . $requestParams['zone'];
            $update->setZone($zone);
            if (\Ddns\Config::zoneNeedsKey($zone)) {
                $update->setKey(\Ddns\Config::getZoneKeyName($zone), \Ddns\Config::getZoneKeySecret($zone));
            }
            $update->deleteRecord($hostForRecord, null, null, 'A');
            $update->addRecord($hostForRecord, 60, 'A', $requestParams['ip']);
            $update->send();

            return $response->getBody()->write('Updated!');
        }
        else {
            return $response->withStatus(403);
        }
    });

    $app->get('/hash', function (Request $request, Response $response) {
        if (array_key_exists('str', $request->getQueryParams())) {
            return $response->getBody()->write(\Ddns\Config::getHashedPassword($request->getQueryParams()['str']));
        } else {
            $response->getBody()->write('Bad Request: Invalid parameters.');
            return $response->withStatus(400);
        }
    });

    $app->run();
