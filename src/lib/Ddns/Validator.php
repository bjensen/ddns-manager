<?php
namespace Ddns;

class Validator {
    public static function validateZone ($zone) {
        if (!self::isValidDomainName($zone)) {
            return false;
        }

        return true;
    }

    public static function validateHost ($host) {
        if (!self::isValidDomainName($host)) {
            return false;
        }

        if (strpos($host, '.') !== FALSE) {
            return false;
        }

        return true;
    }

    public static function isValidDomainName ($domainName) {
        return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domainName)
            && preg_match("/^.{1,253}$/", $domainName)
            && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domainName)   );
    }

    public static function validateIp ($ip) {
        return filter_var($ip, FILTER_VALIDATE_IP);
    }

}