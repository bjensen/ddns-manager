<?php
namespace Ddns;

class Config {
    private static $config = null;

    public static function hasPermission($user, $password, $zone, $host) {
        if (!self::userExists($user)) {
            return false;
        }

        if (!self::isUserPasswordCorrect($user, $password)) {
            return false;
        }

        return self::canChangeHost($user, $zone, $host);
    }

    public static function userExists ($user) {
        $config = self::getConfig();
        return isset($config['users'][$user]);
    }

    public static function isUserPasswordCorrect ($user, $password) {
        $config = self::getConfig();
        return self::checkHashedPassword($password, $config['users'][$user]);
    }

    public static function getHashedPassword ($password) {
        return hash('sha256', self::getSalt() . $password);
    }

    public static function checkHashedPassword ($input, $stored) {
        return hash('sha256', self::getSalt() . $input) == $stored;
    }

    public static function getUserPasswordHash ($user) {
        $config = self::getConfig();
        return $config['users'][$user];
    }

    public static function getZoneKeyName ($zone) {
        $config = self::getConfig();
        $zoneSectionName = self::getZoneSectionName($zone);

        if (!isset($config[$zoneSectionName])) {
            throw new \Exception('Zone does not exist!');
        }

        $zoneConfig = $config[$zoneSectionName];

        return $zoneConfig['.key_name'];
    }

    public static function getZoneKeySecret ($zone) {
        $config = self::getConfig();
        $zoneSectionName = self::getZoneSectionName($zone);

        if (!isset($config[$zoneSectionName])) {
            throw new \Exception('Zone does not exist!');
        }

        $zoneConfig = $config[$zoneSectionName];

        return $zoneConfig['.key_secret'];
    }

    public static function zoneNeedsKey ($zone) {
        $config = self::getConfig();
        $zoneSectionName = self::getZoneSectionName($zone);

        if (!isset($config[$zoneSectionName])) {
            throw new \Exception('Zone does not exist!');
        }

        $zoneConfig = $config[$zoneSectionName];

        return !empty($zoneConfig['.key_name']) && !empty($zoneConfig['.key_secret']);
    }

    public static function canChangeHost ($user, $zone, $host) {
        foreach ([$host, '*'] as $hostEntry) {
            if (self::hostExists($zone, $hostEntry)) {
                // Let the user do it if they can
                if (in_array($user, self::getHostUsers($zone, $hostEntry))) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function hostExists ($zone, $host) {
        $config = self::getConfig();

        if (!self::zoneExists($zone)) {
            return false;
        }

        return isset($config[self::getZoneSectionName($zone)][$host]);
    }

    public static function getHostUsers ($zone, $host) {
        $config = self::getConfig();

        if (!self::hostExists($zone, $host)) {
            throw new \Exception('Host does not exist in zone!');
        }

        return explode(" ", $config[self::getZoneSectionName($zone)][$host]);
    }

    public static function zoneExists ($zone) {
        $config = self::getConfig();
        return isset($config[self::getZoneSectionName($zone)]);
    }

    public static function getGlobal($key) {
        return self::getConfig()['general'][$key];
    }

    public static function getSalt () {
        return self::getGlobal('salt');
    }

    private static function getZoneSectionName ($zone) {
        return 'zone:' . $zone;
    }

    public static function getConfig () {
        if (!self::$config) {
            self::$config = parse_ini_file(__DIR__ . '/../../../config/config.ini', true);
        }

        return self::$config;
    }
}