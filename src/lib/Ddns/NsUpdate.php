<?php
namespace Ddns;

class NsUpdate {
    private $serverAddress, $serverPort;
    private $localAddress, $localPort;
    private $zone;
    private $class;
    private $keyName, $keySecret;
    private $updates = array();

    /**
     * Set the server parameters to be used
     * @param string $address The address (hostname or ip) of the DNS server
     * @param int $port (optional) The port to use (default is 53)
     * @throws \Exception
     */
    public function setServer ($address, $port = null) {
        if (!Validator::isValidDomainName($address)) {
            throw new \Exception('Invalid server address');
        }

        $this->serverAddress = $address;
        $this->serverPort = $port;
    }

    /**
     * Set the local address and port to use (similar to bind)
     * @param $addr string Local Address
     * @param $port int (optional) Local Port to bind to
     */
    public function setLocal ($addr, $port = null) {
        $this->serverAddress = $addr;
        $this->serverPort = $port;
    }

    /**
     * Set the zone
     * @param string $zone Zone
     * @throws \Exception
     */
    public function setZone ($zone) {
        if (!Validator::isValidDomainName($zone)) {
            throw new \Exception('Invalid zone given');
        }

        $this->zone = $zone;
    }


    /**
     * Set the default record class
     * @param string $class Record class
     */
    public function setClass ($class) {
        $this->class = $class;
    }

    /**
     * Set a key to be used in the update
     * @param string $name Name of the key (must match the name in the zone record of the DNS server)
     * @param string $secret The secret to be used
     */
    public function setKey ($name, $secret) {
        $this->keyName = $name;
        $this->keySecret = $secret;
    }

    /**
     * Request a record deletion, filtering on optional parameters
     * @param string $domain the domain to update (eg. home.example.com)
     * @param int $ttl (optional) Time to live in seconds
     * @param string $class (optional) Record class (default is IN)
     * @param null $type (optional) Record type (A, AAAA, etc)
     * @param null $data (optional) The data
     * @throws \Exception
     */
    public function deleteRecord ($domain, $ttl = null, $class = null, $type = null, $data = null) {
        $parts = array('update delete');

        if (empty($domain)) {
            throw new \Exception('Domain must be specified');
        }

        if (!Validator::isValidDomainName($domain)) {
            throw new \Exception('Invalid domain name specified');
        }

        $parts[] = $domain;

        if ($ttl) {
            $parts[] = $ttl;
        }

        if ($class) {
            $parts[] = $class;
        }

        if ($type) {
            $parts[] = $type;
        }

        if ($data) {
            $parts[] = $data;
        }

        $this->updates[] = implode(' ', $parts);
    }

    /**
     * Request a record to be added.
     * @param string $domain Domain name to add
     * @param int $ttl Time to Live in seconds
     * @param string $type Record type (A, AAAA, etc)
     * @param string $data Data field of the record
     * @param string $class (optional) Record class (eg. IN)
     * @throws \Exception
     */
    public function addRecord ($domain, $ttl, $type, $data, $class = null) {
        $parts = array('update add');

        if (empty($domain)) {
            throw new \Exception('Domain must be specified');
        }

        if (!Validator::isValidDomainName($domain)) {
            throw new \Exception('Invalid Domain name specified');
        }

        if (!$ttl || $ttl == '') {
            throw new \Exception('Time to live (TTL) must be specified');
        }

        if (!$type || $type == '') {
            throw new \Exception('Record type must be specified');
        }

        if (!$data || $data == '') {
            throw new \Exception('Data must be provided');
        }

        $parts[] = $domain;
        $parts[] = $ttl;

        if ($class) {
            $parts[] = $class;
        }

        $parts[] = $type;
        $parts[] = $data;

        $this->updates[] = implode(' ', $parts);
    }

    /**
     * Generate the query to be sent to nsupdate
     * @return string Query
     */
    public function generateQuery () {
        $parts = array();

        if ($this->serverAddress) {
            $parts[] = 'server ' . $this->serverAddress . ($this->serverPort ? (' ' . $this->serverPort) : '');
        }

        if ($this->localAddress) {
            $parts[] = 'local ' . $this->localAddress . ($this->localAddress ? (' ' . $this->localPort) : '');
        }

        if ($this->zone) {
            $parts[] = 'zone ' . $this->zone;
        }

        if ($this->class) {
            $parts[] = 'class ' . $this->class;
        }

        if ($this->keyName && $this->keySecret) {
            $parts[] = 'key ' . $this->keyName . ' ' . $this->keySecret;
        }

        $parts[] = implode("\n", $this->updates);
        $parts[] = "send\n";

        return implode("\n", $parts);
    }

    public function send () {
        $descriptorSpecifications = array(
            0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
            1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
            2 => array("pipe", "w")   // stderr is a pipe that the child will write to
        );

        $nsUpdateProcess = proc_open('nsupdate -d', $descriptorSpecifications, $pipes);

        if (is_resource($nsUpdateProcess)) {
            fwrite($pipes[0], $this->generateQuery());
            fclose($pipes[0]);

            $output = stream_get_contents($pipes[1]);
            fclose($pipes[1]);

            $errorOutput = stream_get_contents($pipes[2]);
            fclose($pipes[2]);

            $returnValue = proc_close($nsUpdateProcess);

            // Handle an error
            if ($returnValue != 0) {
                throw new \Exception($output . $errorOutput);
            }
            //Everyting went fine
            else {
                return true;
            }
        }
        else {
            throw new \Exception('Fatal Error: Opening nsupdate process failed.');
        }
    }
}
