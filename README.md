Dynamic DNS Manager
===================

For managing dynamic dns records on any BIND dns server.

==== Installation Instructions ====

See samples folder for sample configurations.

TODO: insert other instructions here.

If you are using SELinux, do the following:

	semanage fcontect -a -t named_cache_t "/var/named/master(/.*)?"
	restorecon -vR /var/named/master