<?php
//include('src/lib/Ddns/NsUpdate.php');
//
//$nsUpdater = new \Ddns\NsUpdate();
//$nsUpdater->setServer('ns.zyn.us');
//$nsUpdater->setKey('ddns_key', '0qdqJtVdQBA7dAmIKOAmebctsOf1C8EbcFWNCZDmrRpv4y6TvdPO6kUFA3udLkEvuZknHwBWsqfc4cZ7Eg6LWw==');
//$nsUpdater->deleteRecord('netbook.i.zyn.us');
//$nsUpdater->addRecord('netbook.i.zyn.us', 60, 'A', '10.2.2.1');
//echo $nsUpdater->generateQuery();

include('src/lib/Ddns/Config.php');
include('src/lib/Ddns/NsUpdate.php');
include('src/lib/Ddns/Validator.php');

//var_dump(\Ddns\Validator::isValidDomainName('localhost'));
//var_dump(\Ddns\Validator::isValidDomainName('zyn.us'));
//var_dump(\Ddns\Validator::isValidDomainName('blah.blah'));

//var_dump(Ddns\Config::canChangeHost('brendon', 'i.zyn.us', 'test2'));

//echo Ddns\Config::getHashedPassword('blah') . PHP_EOL;

//var_dump(Ddns\Config::hasPermission('admin', 'blah1', 'i.zyn.us', 'test22'));

$requestParams = array (
    'user' => 'admin',
    'key' => 'blah',
    'zone' => 'i.zyn.us',
    'host' => 'z',
    'ip' => '10.1.2.2'
);

if (\Ddns\Config::hasPermission($requestParams['user'], $requestParams['key'], $requestParams['zone'], $requestParams['host'])) {
    // Update DNS Record
    $update = new \Ddns\NsUpdate();
    $update->setServer(\Ddns\Config::getGlobal('server_address'));
    $zone = $requestParams['zone'];
    $hostForRecord = $requestParams['host'] . '.' . $requestParams['zone'];
    $update->setZone($zone);
    if (\Ddns\Config::zoneNeedsKey($zone)) {
        $update->setKey(\Ddns\Config::getZoneKeyName($zone), \Ddns\Config::getZoneKeySecret($zone));
    }
    $update->deleteRecord($hostForRecord);
    $update->addRecord($hostForRecord, 60, 'A', $requestParams['ip']);
    echo $update->generateQuery();
    echo 'good';
}
else {
    echo 'no permission';
}